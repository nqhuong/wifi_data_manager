// Call the dataTables jQuery plugin
$(document).ready(function () {
	if ($('#dataTable').html()) {
		let table = $('#dataTable').DataTable({
			select: {
				style: 'multi'
			},
			dom: 'Bfrtip',
			buttons: [
				'copy', 'csv', 'excel', 'pdf', 'print'
			],
			"order": [[5, "desc"]]
		});


		$('#start_date').datepicker({
			format: 'dd-mm-yyyy',
			uiLibrary: 'bootstrap4',
			// size: 'small'
		});
		$('#end_date').datepicker({
			format: 'dd-mm-yyyy',
			uiLibrary: 'bootstrap4',
			// size: 'small'
		});
	}


	// $("#btn_get").click(function () {
	// 	var count = table.rows({selected: true}).data();
	//
	// 	console.log(count[0]);
	// 	console.log(count[1]);
	//
	// 	count.each(function (e) {
	// 		// console.log(e);
	// 	})
	// });

	if ($("#devicesTable").html()) {
		let table = $('#devicesTable').DataTable({
			select: {
				style: 'single'
			},
			"order": [[0, "asc"]]
		});
		//Edit function
		$('#devicesTable').on('click', 'tbody .edit_btn', function () {
			var data_row = table.row($(this).closest('tr')).data();
			$('#edit_modal input[name="id"]').val(data_row[0]);
			$('#edit_modal input[name="name"]').val(data_row[1]);
			$('#edit_modal input[name="des"]').val(data_row[2]);
			$('#edit_modal input[name="mac"]').val(data_row[3]);
			$('#edit_modal input[name="site"]').val(data_row[4]);
		})
		$('#edit_modal #save_btn').click(function () {
			let data = $('#edit_form').serialize();
			$.post('/admin/action/editac', data)
				.done(function (res) {

				})
				.fail(function (e) {

				})
				.always(function () {
					location.reload();
				})
		});

		//Delete function
		$('#devicesTable').on('click', 'tbody .delete_btn', function () {
			var data_row = table.row($(this).closest('tr')).data();
			$('#delete_modal #device_name').html(data_row[1]);
			$('#delete_modal input[name="id"]').val(data_row[0]);
		});
		$('#delete_modal #confirm').click(function () {
			let id = $('#delete_modal input[name="id"]').val();
			$.post('/admin/action/deleteac', {'id': id})
				.done(function (res) {

				})
				.fail(function (e) {

				})
				.always(function () {
					location.reload();
				})
		});
	}

});
