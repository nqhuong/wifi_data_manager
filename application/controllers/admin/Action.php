<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Action extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Accesspoint');
		if (!$this->session->userdata('user')) {
			$res = array(
				'status' => 'error',
				'message' => 'Session expried!'
			);
			echo json_encode($res);
			exit();
		}
	}

	public function editAc()
	{
		$id = $this->input->post('id');
		$name = $this->input->post('name');
		$des = $this->input->post('des');
		$mac = $this->input->post('mac');
		$site = $this->input->post('site');

		$data = array(
			'name' => $name,
			'description' => $des,
			'mac' => $mac,
			'site' => $site
		);
		if ($id != '') {
			$res = $this->Accesspoint->edit($id, $data);
			if ($res) {
				$this->session->set_flashdata('message', 'Ban ghi da duoc cap nhat!');
				$this->session->set_flashdata('message_type', 'success');
				$res = array(
					'status' => 'success',
					'message' => 'Edit success!'
				);
				echo json_encode($res);
				exit();
			}
		}
		$this->session->set_flashdata('message', 'Ban ghi chua duoc cap nhat!');
		$this->session->set_flashdata('message_type', 'danger');
		$res = array(
			'status' => 'error',
			'message' => '!'
		);
		echo json_encode($res);
		exit();
	}

	public function deleteAc()
	{
		$id = $this->input->post('id');
		$res = $this->Accesspoint->delete($id);
		if ($res) {
			$this->session->set_flashdata('message', 'Da xoa ban ghi!');
			$this->session->set_flashdata('message_type', 'success');
			$res = array(
				'status' => 'success',
				'message' => 'Edit success!'
			);
			echo json_encode($res);
			exit();
		}
		$this->session->set_flashdata('message', 'Ban ghi chua duoc xoa!');
		$this->session->set_flashdata('message_type', 'danger');
		$res = array(
			'status' => 'error',
			'message' => '!'
		);
		echo json_encode($res);
		exit();
	}
}
