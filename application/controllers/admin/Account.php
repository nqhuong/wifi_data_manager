<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Account extends CI_Controller
{
	public function login()
	{
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		if ($username and $password)
			$this->__login($username, $password);
		if ($this->session->userdata('user'))
			return redirect('admin/manage');
		else
			$this->load->view('login');
	}

	public function logout()
	{
		$this->session->set_userdata('user', '');
		return redirect('admin/account/login');
	}

	public function __login($username, $password)
	{
		$this->load->model('User');
		$user = $this->User->check_login($username, $password);
		if ($user) {
			$this->session->set_userdata('user', json_decode(json_encode($user), true));
		}
	}
}


