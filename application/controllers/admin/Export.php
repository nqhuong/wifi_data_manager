<?php


class Export extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('Excel');
		$this->load->model('Data');
		if (!$this->session->userdata('user')) {
			redirect('admin/account/login');
		}
	}

	// create xlsx
	public function index()
	{
		// create file name
		$fileName = 'data-' . time() . '.xlsx';

		$empInfo = $this->Data->get_all();
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->setActiveSheetIndex(0);
		// set Header
		$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'ID');
		$objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Ten');
		$objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Dien thoai');
		$objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Email');
		$objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Dia chi');
		$objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Thoi gian');
		// set Row
		$rowCount = 2;
		foreach ($empInfo->result() as $element) {
			$objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $element->id);
			$objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $element->name);
			$objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $element->phone);
			$objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $element->mail);
			$objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $element->address);
			$objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $element->time);
			$rowCount++;
		}
		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
		$objWriter->save(APPPATH . '/upload/' . $fileName);
		// download file
		header("Content-Type: application/vnd.ms-excel");
		redirect(APPPATH . $fileName);
	}
}
