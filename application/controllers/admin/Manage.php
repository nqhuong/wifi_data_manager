<?php


class Manage extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('user')) {
			redirect('admin/account/login');
		}
		$this->load->model('Accesspoint');
		$this->load->model('Data');
	}

	public function index()
	{
		$this->load->view('master');
	}

	public function devices()
	{
		$res = $this->Accesspoint->get_all();
		$data = array(
			'recs' => $res
		);
		$this->load->view('devices', $data);
	}

	public function data()
	{

		$sd = $this->input->get('start_date') ? $this->input->get('start_date') : date('01-m-yy', time());
		$ed = $this->input->get('start_date') ? $this->input->get('end_date') : date('t-m-yy', time());

		$condition = array(
			'time >=' => date('yy-m-d 00:00:00', strtotime($sd)),
			'time <=' => date('yy-m-d 00:00:00', strtotime($ed)),
		);

		$ap_id = $this->input->get('ap');
		if ($ap_id != '')
			$condition['ac_id'] = $ap_id;

		$res = $this->Data->get_all($condition);
//		var_dump($this->db->last_query());
//		die();
		$aps = $this->Accesspoint->get_all();
		$data = array(
			'recs' => $res,
			'aps' => $aps,
			'filter' => array(
				'sd' => $sd,
				'ed' => $ed,
				'ap_id' => $ap_id
			)
		);
		$this->load->view('data', $data);
	}
}
