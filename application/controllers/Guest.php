<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once APPPATH . '/third_party/vendor/autoload.php';

class Guest extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->config('unifi');
		$this->load->model('Data');
		$this->load->model('Accesspoint');
	}

	public function s($site)
	{
		if ($this->input->get('id'))
			$this->session->set_userdata('id', $this->input->get('id'));
		if ($this->input->get('ap'))
			$this->session->set_userdata('ap', $this->input->get('ap'));
		$this->load->view('splash/index');
	}

	public function connect()
	{
		$mac = $this->session->userdata('id');
		$ap = $this->session->userdata('ap');

		$name = $this->input->post('name');
		$mail = $this->input->post('email');
		$phone = $this->input->post('phone');
		$address = $this->input->post('address');
		$ug = $_SERVER['HTTP_USER_AGENT'];

		$duration = $this->config->item('duration');
		$site_id = $this->config->item('site_id');
		$controlleruser = $this->config->item('username');
		$controllerpassword = $this->config->item('password');
		$controllerurl = $this->config->item('controller_url');
		$controllerversion = $this->config->item('controller_version');;
		$debug = $this->config->item('debug');

		$ac = $this->Accesspoint->get(array('mac' => $ap));

		if ($name and $phone) {
			$data = array(
				'name' => $name,
				'phone' => $phone,
				'mail' => $mail,
				'address' => $address,
				'user_agent' => $ug,
				'ac_id' => $ac->id
			);

			$this->Data->insert($data);

			$unifi_connection = new UniFi_API\Client($controlleruser, $controllerpassword, $controllerurl, $site_id, $controllerversion);
			$set_debug_mode = $unifi_connection->set_debug($debug);
			$loginresults = $unifi_connection->login();
			$auth_result = $unifi_connection->authorize_guest($mac, $duration, $up = null, $down = null, $MBytes = null, $ap);
			$this->load->view('splash/wait');
		} else {
			$this->load->view('splash/index');
		}
	}
}
