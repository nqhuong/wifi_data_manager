<?php

defined('BASEPATH') or exit('No direct script access allowed');
/*
 * Thoi gian cap quyen su dung
 * */
$config['duration'] = 30;

/*
 * Site id  (https://1.1.1.1:8443/manage/site/<site_ID>/devices/1/50)
 * */
$config['site_id'] = 'g3liute2';

/*
 * Tai khoan dang nhap controller
 * */
$config['username'] = 'imusdev';

/*
 * Mat khau dang nhap controller
 * */
$config['password'] = 'imusdev@6688';

/*
 * Dia chi url controller
 * */
$config['controller_url'] = 'https://connect.fpt.net:8443';

/*
 * Controller version
 * */
$config['controller_version'] = '5.12.35';

/*
 * Debug
 * */
$config['debug'] = false;
