<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Data extends CI_Model
{
	private $table = 'data';

	public function insert($data)
	{
		$this->db->insert($this->table, $data)->result();
	}

	public function get_all($condition = null)
	{
		if ($condition)
			$this->db->where($condition);
		$this->db->select('data.id, data.name, phone, mail, address, user_agent, time, accesspoints.name as ac_id');
		$this->db->join('accesspoints', 'accesspoints.id = data.ac_id');
		$this->db->order_by('data.id','ASC');
		return $this->db->get($this->table)->result();
	}
}
