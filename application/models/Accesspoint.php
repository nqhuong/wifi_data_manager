<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Accesspoint extends CI_Model
{

	private $table = 'accesspoints';

	function get_all()
	{
		return $this->db->get($this->table)->result();
	}

	function get($condition)
	{
		if ($condition)
			$this->db->where($condition);

		return $this->db->get($this->table)->row();
	}

	function edit($id, $data)
	{
		$this->db->where(array('id' => $id));
		return $this->db->update($this->table, $data);
	}

	function delete($id)
	{
		$this->db->where(array('id' => $id));
		return $this->db->delete($this->table);
	}
}
