<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends CI_Model
{
	private $table = 'users';

	public function check_login($username, $password)
	{
		$user = $this->db->get_where($this->table, array('username' => $username), 1)->row();
		if ($user and password_verify($password, $user->password)) {
			return $user;
		} else {
			return false;
		}
	}
}
