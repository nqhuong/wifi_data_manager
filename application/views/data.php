<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<meta name="viewport" content="width=devic`e-width, initial-scale=1, shrink-to-fit=no"/>
	<meta name="description" content=""/>
	<meta name="author" content=""/>
	<title>Dashboard - SB Admin</title>
	<link href="<?php echo base_url() ?>assets/css/styles.css" rel="stylesheet"/>
	<link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet"
		  crossorigin="anonymous"/>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/all.min.js"
			crossorigin="anonymous"></script>
</head>
<body class="sb-nav-fixed">
<nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
	<a class="navbar-brand" href="/admin/manage">Wifi Data Manager</a>
	<button class="btn btn-link btn-sm order-1 order-lg-0" id="sidebarToggle" href="#"><i class="fas fa-bars"></i>
	</button>
	<!-- Navbar Search-->
	<form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
		<div class="input-group">
			<input class="form-control" type="text" placeholder="Search for..." aria-label="Search"
				   aria-describedby="basic-addon2"/>
			<div class="input-group-append">
				<button class="btn btn-primary" type="button"><i class="fas fa-search"></i></button>
			</div>
		</div>
	</form>
	<!-- Navbar-->
	<ul class="navbar-nav ml-auto ml-md-0">
		<li class="nav-item dropdown">
			<a class="nav-link dropdown-toggle" id="userDropdown" href="#" role="button" data-toggle="dropdown"
			   aria-haspopup="true" aria-expanded="false"><i class="fas fa-user fa-fw"></i></a>
			<div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
				<a class="dropdown-item" href="#">Settings</a>
				<a class="dropdown-item" href="#">Activity Log</a>
				<div class="dropdown-divider"></div>
				<a class="dropdown-item" href="login.html">Logout</a>
			</div>
		</li>
	</ul>
</nav>
<div id="layoutSidenav">
	<div id="layoutSidenav_nav">
		<nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
			<div class="sb-sidenav-menu">
				<div class="nav">
					<div class="sb-sidenav-menu-heading">Core</div>
					<a class="nav-link" href="/admin/manage">
						<div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
						Dashboard
					</a>
					<div class="sb-sidenav-menu-heading">Menu</div>
					<a class="nav-link" href="/admin/manage/devices">
						<div class="sb-nav-link-icon"><i class="fas fa-chart-area"></i></div>
						Thiet bi
					</a>
					<a class="nav-link" href="/admin/manage/data">
						<div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
						Du lieu
					</a>
				</div>
			</div>
			<div class="sb-sidenav-footer">
				<div class="small">Logged in as:</div>
				<?php echo $this->session->userdata('user')['username'] ?>
			</div>
		</nav>
	</div>
	<div id="layoutSidenav_content">
		<main>
			<div class="container-fluid">
				<h1 class="mt-4">Dữ liệu</h1>
				<ol class="breadcrumb mb-4">
					<li class="breadcrumb-item"><a href="/admin/manage">Dashboard</a></li>
					<li class="breadcrumb-item active">Dữ liệu</li>
				</ol>
				<div class="card-mb-4">
					<form class="container-flui">
						<div class="d-flex row">
							<div class="col-md-3">
								<input name="start_date" id="start_date" value="<?php echo $filter['sd'] ?>">
							</div>
							<div class="col-md-3">
								<input name="end_date" id="end_date" value="<?php echo $filter['ed'] ?>">
							</div>
							<div class="col-md-4">
								<div class="gj-datepicker gj-datepicker-bootstrap gj-unselectable input-group input-group-sm">
									<select class="form-control" style="font-size: 0.85rem" name="ap">
										<option value="">-- Wifi --</option>
										<?php foreach ($aps as $ap) { ?>
											<option value="<?php echo $ap->id ?>"
													<?php if ($filter['ap_id'] == $ap->id) echo 'selected' ?> ><?php echo $ap->name ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
							<div class="col-md-2">
								<input type="submit" class="btn-small btn-primary" value="Search">
							</div>
						</div>
					</form>
				</div>
				<div class="card mb-4">
					<div class="card-body">
						<div class="table-responsive" id="btn_get">
							<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
								<thead>
								<tr>
									<th>Tên</th>
									<th>Điện thoại</th>
									<th>Email</th>
									<th>Địa chỉ</th>
									<th>Thiết bị</th>
									<th>Thời gian</th>
									<th>Wifi</th>
								</tr>
								</thead>
								<tbody>
								<?php foreach ($recs as $r) { ?>
									<tr>
										<td><?php echo $r->name ?></td>
										<td><?php echo $r->phone ?></td>
										<td><?php echo $r->mail ?></td>
										<td><?php echo $r->address ?></td>
										<td><?php echo $r->user_agent ?></td>
										<td><?php echo $r->time ?></td>
										<td><?php echo $r->ac_id ?></td>
									</tr>
								<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</main>
		<footer class="py-4 bg-light mt-auto">
			<div class="container-fluid">
				<div class="d-flex align-items-center justify-content-between small">
					<div class="text-muted">Copyright &copy; Your Website 2020</div>
					<div>
						<a href="#">Privacy Policy</a>
						&middot;
						<a href="#">Terms &amp; Conditions</a>
					</div>
				</div>
			</div>
		</footer>
	</div>
</div>
<script src="https://code.jquery.com/jquery-3.5.1.min.js" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min.js"
		crossorigin="anonymous"></script>
<script src="<?php echo base_url() ?>assets/js/scripts.js"></script>
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>-->
<!--<script src="--><?php //echo base_url() ?><!--assets/demo/chart-area-demo.js"></script>-->
<!--<script src="--><?php //echo base_url() ?><!--assets/demo/chart-bar-demo.js"></script>-->
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/select/1.3.1/js/dataTables.select.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
<!--https://code.jquery.com/jquery-3.5.1.js-->
<!--https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js-->
<!--https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js-->
<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.print.min.js"></script>
<!--add-->
<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
<link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css"/>
<!--end add-->
<script src="<?php echo base_url() ?>assets/demo/datatables-demo.js"></script>
<!--https://code.jquery.com/jquery-3.5.1.js-->
<style rel="stylesheet">
	table.dataTable tbody tr.selected {
		color: white !important;
		background-color: #a6b4cd !important;
	}
</style>
</body>
</html>
