<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
	<meta name="description" content=""/>
	<meta name="author" content=""/>
	<title>Dashboard - SB Admin</title>
	<link href="<?php echo base_url() ?>assets/css/styles.css" rel="stylesheet"/>
	<link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet"
		  crossorigin="anonymous"/>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/all.min.js"
			crossorigin="anonymous"></script>
</head>
<body class="sb-nav-fixed">
<nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
<a class="navbar-brand" href="/admin/manage">Wifi Data Manager</a>
	<button class="btn btn-link btn-sm order-1 order-lg-0" id="sidebarToggle" href="#"><i class="fas fa-bars"></i>
	</button>
	<!-- Navbar Search-->
	<form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
		<div class="input-group">
			<input class="form-control" type="text" placeholder="Search for..." aria-label="Search"
				   aria-describedby="basic-addon2"/>
			<div class="input-group-append">
				<button class="btn btn-primary" type="button"><i class="fas fa-search"></i></button>
			</div>
		</div>
	</form>
	<!-- Navbar-->
	<ul class="navbar-nav ml-auto ml-md-0">
		<li class="nav-item dropdown">
			<a class="nav-link dropdown-toggle" id="userDropdown" href="#" role="button" data-toggle="dropdown"
			   aria-haspopup="true" aria-expanded="false"><i class="fas fa-user fa-fw"></i></a>
			<div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
				<a class="dropdown-item" href="#">Settings</a>
				<a class="dropdown-item" href="#">Activity Log</a>
				<div class="dropdown-divider"></div>
				<a class="dropdown-item" href="login.html">Logout</a>
			</div>
		</li>
	</ul>
</nav>
<div id="layoutSidenav">
	<div id="layoutSidenav_nav">
		<nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
			<div class="sb-sidenav-menu">
				<div class="nav">
					<div class="sb-sidenav-menu-heading">Core</div>
					<a class="nav-link" href="/admin/manage">
						<div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
						Dashboard
					</a>
					<div class="sb-sidenav-menu-heading">Menu</div>
					<a class="nav-link active" href="/admin/manage/devices">
						<div class="sb-nav-link-icon"><i class="fas fa-chart-area"></i></div>
						Thiết bị
					</a>
					<a class="nav-link" href="/admin/manage/data">
						<div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
						Dữ liệu
					</a>
				</div>
			</div>
			<div class="sb-sidenav-footer">
				<div class="small">Logged in as:</div>
				<?php echo $this->session->userdata('user')['username'] ?>
			</div>
		</nav>
	</div>
	<div id="layoutSidenav_content">
		<main>
			<div class="container-fluid">
				<h1 class="mt-4">Thiết bị</h1>
				<ol class="breadcrumb mb-4">
					<li class="breadcrumb-item"><a href="/admin/manage">Dashboard</a></li>
				<li class="breadcrumb-item active">Thiết bị</li>
				</ol>
				<?php if ($this->session->flashdata('message')) { ?>
					<div class="card mb-4">
						<div class="alert alert-<?php echo $this->session->flashdata('message_type') ?>" role="alert">
							<?php echo $this->session->flashdata('message') ?>
						</div>
					</div>
				<?php } ?>
				<div class="card mb-4">
					<div class="card-body">
						<div class="table-responsive" id="btn_get">
							<table class="table table-bordered" id="devicesTable" width="100%" cellspacing="0">
								<thead>
								<tr>
									<th>ID</th>
									<th>Tên</th>
									<th>Mô tả</th>
									<th>MAC</th>
									<th>Site</th>
									<th></th>
								</tr>
								</thead>
								<tbody>
								<?php foreach ($recs as $r) { ?>
									<tr>
										<td><?php echo $r->id ?></td>
										<td><?php echo $r->name ?></td>
										<td><?php echo $r->description ?></td>
										<td><?php echo $r->mac ?></td>
										<td><?php echo $r->site ?></td>
										<td>
											<button class="btn btn-primary edit_btn" data-toggle="modal"
													data-target="#edit_modal">Sửa
											</button>
											<button class="btn btn-danger delete_btn" data-toggle="modal"
													data-target="#delete_modal">Xóa
											</button>
										</td>
									</tr>
								<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</main>
		<footer class="py-4 bg-light mt-auto">
			<div class="container-fluid">
				<div class="d-flex align-items-center justify-content-between small">
					<div class="text-muted">Copyright &copy; Your Website 2020</div>
					<div>
						<a href="#">Privacy Policy</a>
						&middot;
						<a href="#">Terms &amp; Conditions</a>
					</div>
				</div>
			</div>
		</footer>
	</div>
</div>


<!-- Modal edit -->
<div class="modal fade" id="edit_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
	 aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">Sửa thông tin thiết bị:</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form class="form-group" id="edit_form">
					<input hidden name="id">
					<input class="form-control" name="name">
					<input class="form-control" name="des">
					<input class="form-control" name="mac">
					<input class="form-control" name="site">
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Hủy</button>
				<button type="button" class="btn btn-primary" id="save_btn">Xác nhận</button>
			</div>
		</div>
	</div>
</div>

<!--Modal delete-->

<!-- Modal edit -->
<div class="modal fade" id="delete_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
	 aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">Bạn có muốn xóa <span id="device_name"></span> ?</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
				<input hidden name="id">
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Hủy</button>
				<button type="button" class="btn btn-primary" id="confirm">Xác nhận</button>
			</div>
		</div>
	</div>
</div>

<script src="https://code.jquery.com/jquery-3.5.1.min.js" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min.js"
		crossorigin="anonymous"></script>
<script src="<?php echo base_url() ?>assets/js/scripts.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/select/1.3.1/js/dataTables.select.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
<script src="<?php echo base_url() ?>assets/demo/datatables-demo.js"></script>

<!--https://code.jquery.com/jquery-3.5.1.js-->
<style rel="stylesheet">
	table.dataTable tbody tr.selected {
		color: white !important;
		background-color: #a6b4cd !important;
	}
</style>
</body>
</html>
